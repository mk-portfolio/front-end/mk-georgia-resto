'use strict';

/*===== Declaring Variable =====*/
const checkbox = document.getElementById('check');

const menuLink = document.querySelectorAll('.navbar-navi-right__list-link');

/*/////////////////////////////////////////////////////*/

/*===== Functions =====*/
const enableDisableScrollBar = () => {
    checkbox.checked ? document.body.style.overflow = 'hidden' : document.body.style.overflow = 'auto';
}

/*/////////////////////////////////////////////////////*/

/*===== Event Handler =====*/
// Disabling Scrollbar when menu is active
checkbox.addEventListener('click', enableDisableScrollBar);

// Closing Menu when link is Clicked
menuLink.forEach(link => {
    link.addEventListener('click', () => {
        checkbox.checked = false; // Toggle the checkbox's checked state to close the menu
        enableDisableScrollBar(); // Enable Scrollbar when menu is inactive
    });
});

// Closing Menu when "ESC" key is Clicked
document.addEventListener('keydown', e => {
    if(e.key === 'Escape' && checkbox.checked){
        checkbox.checked = false; // Toggle the checkbox's checked state to close
        enableDisableScrollBar(); // Enable Scrollbar when menu is inactive
    }
});